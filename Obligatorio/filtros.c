/**
@file filtro.c
@author M.Visca
@date 11/4/2017 15:35
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pgm.h"

#define max(a,b) ((a > b) ? a:b)
#define min(a,b) ((a < b) ? a:b)

int clipp(int a, int b, int x){
  return min(a, max(b, x));
}

void copia(int in_img[], int width, int heigth, int out_img[]){
	int m, n;

	for(m = 0; m < heigth; m++)
		for(n = 0; n < width; n++)
			out_img[m * width + n] = in_img[m * width + n];  /*Recorro y asigno pixel a pixel*/

}/*copia*/

void negativo(int in_img[], int width, int heigth, int maxval, int out_img[]){
	int i;

	for(i = 0; i < width*heigth; i++)
		out_img[i] = maxval - in_img[i];									/*Recorro y asigno la resta del maxval menos el pixel */

}/*negativo*/

void reflejo(int in_img[], int width, int heigth, int type, int out_img[]){
	int m, n;

	if(type == 0){
		for(n = 0; n < width/2; n++){										/*Recorro hasta la mitad del ancho, pivot*/
			for(m = 0; m < heigth; m++){
				out_img[m*width + n] = in_img[m*width + (width -1 - n)];	/*A c/pixel le asigno el pixel opuesto*/
				out_img[m*width + (width -1 -n)] = in_img[m*width + n];		/* en la misma fila*/
			}
		}
	}
	else if(type == 1){
		for(m = 0; m < heigth/2; m++){								/*Recorro hasta la mitad de la altura, pivot*/
			for(n = 0; n < width; n++){
				out_img[m*width + n] = in_img[(heigth -1- m)*width + n];	/*A c/pixel le asigno el opuesto*/
				out_img[(heigth -1- m)*width + n] = in_img[m*width + n];	/*en la misma columna*/
			}
		}
	}
	else if(type == 2){
		for(m = 0; m < heigth/2; m++){							/*Recorro hasta la mitad de la altura, pivot*/
			for(n = 0; n < width; n++){
				out_img[m*width + n] = in_img[(heigth -1- m)*width + (width - 1 - n)]; /*A c/pixel le asigno el opuesto*/
				out_img[(heigth -1- m)*width + (width - 1 - n)] = in_img[m*width + n]; /*en la fila y col opuesta*/
			}
		}
	}
	else{
		printf("¡ERROR!, Parámetro inválido\a\n");
		exit(0);
	}
}/*reflejo*/

void contraste(int in_img[], int width, int heigth, int maxval, int out_img[]){
	int i, max, min;
	max = 0;				/*min pixel*/
	min = 255;			/*max pixel*/

	for(i = 0; i < width*heigth; i++)   /*Recorro en busca del max pixel presente*/
		if(max < in_img[i])
			max = in_img[i];

	for(i = 0; i < width*heigth; i++)		/*Recorro en busca del min pixel presente*/
		if(min > in_img[i])
			min = in_img[i];

	for(i = 0; i < width*heigth; i++)
		out_img[i] = round((float) (maxval * (in_img[i] - min)) / (max - min));	/*Calculo y asigno*/

}/*contraste*/

void promedio(int img[], int width, int heigth, int maxval, int w, int out_img[]){

	int m, n, j, i, twgt;

	twgt = (2*w + 1)*(2*w +1);		/*Peso neto*/

	for(m = 0; m < heigth; m++)
		for(n = 0; n < width; n++){
			float val = 0;

			for(i = m-w; i <= m+w; i++)
				for(j = n-w; j <= n+w; j++){
					int ii = clipp(heigth - 1, 0, i);
					int jj = clipp(width - 1, 0, j);

					val += img[ii * width + jj];  /*"Producto convolutivo"*/
				} /*for en j*/

			out_img[m * width + n] = round(val/twgt); /*Redondeo el pixel y asigno*/
		} /*for en n*/

}/*pro*/

void bordes(int in_img[], int width, int heigth, int maxval, int p, int out_img[]){
	int m, n;

	for(m = 0; m < heigth; m++)
		for(n = 0; n < width; n++){
			float val = 0, vx = 0, vy = 0;					/*val: Valor del gradiente, vx = valor acumulado de la deriv respecto a x*/
											                        /*vy = valor acumulado de la derivada respecto a y*/
																							/*di, dj correciones de i y j para que se acceda bien a dx y dy*/
				int ii = clipp(heigth -1, 0, m+1);
				int iii = clipp(heigth -1, 0, m-1);

        int jj = clipp(width -1, 0, n+1);
        int jjj = clipp(width -1, 0, n-1);

				switch (p) {
					case  0:
            vx = in_img[m*width + (jj)] - in_img[m*width + (jjj)];	/*"Prod. convolutivo" de dx con la ventana de pixeles*/
						break;

					case 1:
            vy = in_img[(ii)*width + n] - in_img[(iii)*width + n];	/*"Prod. convolutivo" de dy con la ventana de pixeles*/
						break;

					case 2:
            vx = in_img[m*width + (jj)] - in_img[m*width + (jjj)];
						vy = in_img[(ii)*width + n] - in_img[(iii)*width + n];
						break;
					default:
						printf("¡ERROR! parametro invalido, verifique.\n");
						exit(0);
				} /*switch*/

			val = sqrt(vx*vx + vy*vy);																				/*Calculo y asignacion del gradiente*/
			
			out_img[m * width + n] = ( val > maxval) ? maxval:round(val);			/*si el valor del gradiente es mayor al max. val. de pixel se trunca a max. val.*/

		}/*for en n*/

} /*bordes*/
