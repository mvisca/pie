/**
@file basic.h
@author M.Visca
@date 31/03/2017
@brief
Conjunto de funciones para filtrar imagenes.
*/

/**
*Genera una copia una de la imagen.
*@param in_img: arreglo de pixeles a copiar
*@param width: ancho de la imagen
*@param heigth: altura de la imagen
*@param out_img: arreglo de pixeles, donde se realiza la copia.
*/
void copia(int in_img[], int width, int heigth, int out_img[]);

/**
*Genera el negativo de la imagen.
*@param in_img: arreglo de pixeles a copiar
*@param width: ancho de la imagen
*@param heigth: altura de la imagen
*@param maxval: maximo valor de pixel
*@param out_img: arreglo de pixeles, donde se almacena el negativo.
*/
void negativo(int in_img[], int width, int heigth, int maxval, int out_img[]);

/**
*Genera el reflejo de la imagen.
*@param in_img: arreglo de pixeles a copiar
*@param width: ancho de la imagen
*@param heigth: altura de la imagen
*@param type: tipo de reflejo. 0: horizontal, 1: vertical, 2: central
*@param out_img: arreglo de pixeles, donde se realiza la copia.
*/
void reflejo(int in_img[], int width, int heigth, int type, int out_img[]);
