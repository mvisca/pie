#include <stdlib.h>
#include <stdio.h>
#include "pgm.h"

void copia(int in_img[], int width, int heigth, int out_img[]){
	int m, n;

	for(m = 0; m < heigth; m++)
		for(n = 0; n < width; n++)
			out_img[m * width + n] = in_img[m * width + n];

}

void negativo(int in_img[], int width, int heigth, int maxval, int out_img[]){
	int i;

	for(i = 0; i < width*heigth; i++)
		out_img[i] = maxval - in_img[i];

}

void reflejo(int in_img[], int width, int heigth, int type, int out_img[]){
	int m, n;

	if(type == 0){
		for(n = 0; n < width/2; n++){
			for(m = 0; m < heigth; m++){
				out_img[m*width + n] = in_img[m*width + (width -1 - n)];
				out_img[m*width + (width -1 -n)] = in_img[m*width + n];
			}
		}
	}
	else if(type == 1){
		for(m = 0; m < heigth/2; m++){
			for(n = 0; n < width; n++){
				out_img[m*width + n] = in_img[(heigth -1- m)*width + n];
				out_img[(heigth -1- m)*width + n] = in_img[m*width + n];
			}
		}
	}
	else if(type == 2){
		for(m = 0; m < heigth/2; m++){
			for(n = 0; n < width; n++){
				out_img[m*width + n] = in_img[(heigth -1- m)*width + (width - 1 - n)];
				out_img[(heigth -1- m)*width + (width - 1 - n)] = in_img[m*width + n];
			}
		}
	}
	else{
		printf("¡ERROR!, Parámetro inválido\a\n");
		exit(0);
	}
}
