/**
@file basic.h
@author M.Visca
@date 31/03/2017
@brief
Conjunto de funciones para filtrar imagenes.
*/

/**
*Genera una copia una de la imagen.
*@param in_img: arreglo de pixeles a copiar
*@param width: ancho de la imagen
*@param heigth: altura de la imagen
*@param out_img: arreglo de pixeles, donde se realiza la copia.
*/
void copia(int in_img[], int width, int heigth, int out_img[]);

/**
*Genera el negativo de la imagen.
*@param in_img: arreglo de pixeles a calcular el negativo
*@param width: ancho de la imagen
*@param heigth: altura de la imagen
*@param maxval: maximo valor de pixel
*@param out_img: arreglo de pixeles, donde se almacena el negativo.
*/
void negativo(int in_img[], int width, int heigth, int maxval, int out_img[]);

/**
*Genera el reflejo de la imagen.
*@param in_img: arreglo de pixeles a calcular el reflejo
*@param width: ancho de la imagen
*@param heigth: altura de la imagen
*@param type: tipo de reflejo. 0: horizontal, 1: vertical, 2: central
*@param out_img: arreglo de pixeles de salida.
*/
void reflejo(int in_img[], int width, int heigth, int type, int out_img[]);

/**
*Contrasta la imagen, convierte las partes mas oscuras a negro y las claras a blanco
*@param in_img: arreglo de pixeles a contrastar
*@param width: ancho de la imagen
*@param heigth: altura de la imagen
*@param maxval: maximo valor de pixel
*@param out_img: arreglo de pixeles de salida.
*/
void contraste(int in_img[], int width, int heigth, int maxval, int out_img[]);

/**
*Verifica que b <= x <= a, si x < b retorna b, si x > a retorna a.
*usada para controlar que los indices no se salgan de rango en los bordes
*@param a: limite sup
*@param b: limite inf
*@param x: valor a verificar
*/
int clipp(int a, int b, int x);


/**
*Calcula el promedio de un pixel de acuerdo con sus vecinos en una ventana de tamaño [2w + 1]*[2w + 1]
*@param in_img: arreglo de pixeles a promediar
*@param width: ancho de la imagen
*@param heigth: altura de la imagen
*@param maxval: maximo valor de pixel
*@param w: distancia al pixel central
*@param out_img: arreglo de pixeles de salida.
*/
void promedio(int img[], int weigth, int heigth, int maxval, int w, int out_img[]);

/**
*Reconoce los bordes mediante el uso de las derivadas en x e y, discretizadas
*@param in_img: arreglo de pixeles a procesar
*@param width: ancho de la imagen
*@param heigth: altura de la imagen
*@param maxval: maximo valor de pixel
*@param p: tipo de derivada. 0: horizontal, 1: vertical, 2: gradiente.
*@param out_img: arreglo de pixeles de salida.
*/
void bordes(int in_img[], int width, int heigth, int maxval, int p, int out_img[]);
