/**
@file main.c
@author M. Visca
@date 31/03/2017
@brief
Funcion main, encargada de aplicar el filtro correspondiente.
*/

/*
Librerias
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "filtros.h"
#include "pgm.h"

int img[ANCHO*ALTO]; 			/*!< Array de pixeles, para la imagen de entrada.*/
int out_img[ANCHO*ALTO];  /*!< Array de pixeles, para la imagen de salida. */

/**
Toma exactamente cuatro argumentos:
@param arg1: comando a realizar(copia, negativo, reflejo)
@param arg2: parametro para cada comando
@param arg3: imagen de entrada
@param arg4: imagen de salida
*/
int main(int argc, char *argv[]){
	char *comando, *entrada, *salida; 	/* Declaración de variables como punteros */
	int parametro;											/* Declaración parametro como variable entera */

	if(argc < 2 || argc > 5){																		 /* Chequeo si no se pasaron args. o si se pasaron mas de los permitidos*/
		printf("¡ERROR!, Cantidad de argumentos inválida.\a\n");
		exit(0);
	}
	else if(argc < 5){																					 /* Chequeo que se pasen los cuatro args. necesarios */
		printf("¡ERROR!, Faltan argumentos, verifique.\a\n");
		exit(0);
	}

	/* Asigno los punteros y el parámetro*/
	comando = argv[1];
	entrada = argv[3];
	salida  = argv[4];
	parametro = atoi(argv[2]);

	leer_archivo_pgm(entrada, img);																	/*Cargo la imagen en el array img */

	if(strcmp(comando, "copia") == 0)																/*Decide el filtro a aplicar, según el comando*/
		copia(img, ANCHO, ALTO, out_img);

	else if(strcmp(comando, "negativo") == 0)
		negativo(img, ANCHO, ALTO, MAXVAL, out_img);

	else if(strcmp(comando, "reflejo") == 0)
		reflejo(img, ANCHO, ALTO, parametro, out_img);

	else if(strcmp(comando, "contraste") == 0)
		contraste(img, ANCHO, ALTO, MAXVAL, out_img);

	else if(strcmp(comando, "promedio")	== 0)
		promedio(img, ANCHO, ALTO, MAXVAL, parametro, out_img);

	else if(strcmp(comando, "bordes")	== 0)
		bordes(img, ANCHO, ALTO, MAXVAL, parametro, out_img);

	else{
		printf("Comando Inválido\a\n");
		exit(0);
	}
	escribir_archivo_pgm(out_img, ANCHO, ALTO, MAXVAL, salida);		/*Escribo el array de pixeles de salida en disco*/
	return 0;
}
