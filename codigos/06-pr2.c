#include <stdio.h>
#include <stdlib.h>
void main(int argc, char **argv){
	int tope = sizeof(int)*8;
	int num,vec[tope];
	unsigned int msk = 1;

	num = atoi(argv[1]);
	printf("%d\n", tope);
	for(int i= tope -1 ; i >= 0; i--){
		vec[i] = num & msk;
		num >>= 1;

	}
	for(int j=0; j<tope;j++){
		printf("%u",vec[j]);
	}
	printf("\n");

}
