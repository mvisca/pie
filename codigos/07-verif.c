#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX_DIG 7
int verif(int ci[MAX_DIG]){
  int s, r, v;
  int coef[MAX_DIG];

  // Coeficientes multiplicativos
  coef[0] = 2; coef[1] = 9; coef[2] = 8;
  coef[3] = 7; coef[4] = 6; coef[5] = 3;
  coef[6] = 4;

  // s: resultado del producto interno de los digitos de CI con los coefs
  s=0;
  for(int i=0; i < MAX_DIG; i++)
    s += coef[i]*ci[i];

  // r: menor múltiplo de 10 mayor a s
  r = (s - (s % 10)) +10;
  // v: digito verificador
  v = r-s;

  return v;
  }

int main(int argc, char **argv){
  /* Dado el numero de CI(argv[1]), se lo descompone en sus digitos
  y se llena el array CI, para luego pasarlo como parametro a la funcion verif
  */
  int CI[MAX_DIG];
  int ver;

  int j=0;
  for (int i = pow(10, MAX_DIG-1); i>0; i/=10){
     CI[j] =((atoi(argv[1])/i) % 10);
     j++;
  }

  ver = verif(CI);
  printf("Numero verficador = %d \n", ver);
  return 0;
}
