#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int gaussiana(float x, float mu, float sigma)
{
  float y;
  float num, den;

  den = sigma*sqrt(2*M_PI);
  num = exp(-0.5*pow((x-mu)/sigma,2));
  y = num/den;

  printf("%1.10f\n", y);

  return 0;
}

int main(int argc, char **argv)
{
  gaussiana(atof(argv[1]), atof(argv[2]), atof(argv[3]));
  return 0;
}
