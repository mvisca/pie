#include <stdio.h>
#include <stdlib.h>

int plaza2orig(float pplaza, float iva){
  float orig;

  orig = pplaza/(1+iva);
  return orig;
}

int orig2plaza(float orig, float iva){
  float pplaza;

  pplaza = (1 + iva)*orig;
  return pplaza;
}

int main(int argc, char **argv){
  float res;
  int dec;

  printf("Opciones: 1: orig2plaza 2: plaza2orig ");
  scanf("%d", &dec);

  switch (dec) {
    case 1:{
        res = orig2plaza(atof(argv[1]), atof(argv[2]));
        break;
      }
    case 2:{
        res = plaza2orig(atof(argv[1]), atof(argv[2]));
        break;
      }
  }
  printf("$ %.2f \n", res);
}
