#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "/home/mvisca/Documentos/pie/codes/01-mod.c"

int main(){
  int numero, aux, cont;

  // Escanea un numero desde la consola
  printf("Ingrese un numero: ");
  scanf("%i", &numero);

  cont = 0;
  aux = numero;

  // Cuenta la cantidad de digitos
  // conocer la cantidad de digitos es saber por cuanto dividir
  // para obtener el primer digito
  while(aux > 10){
    aux = aux/10;
    cont++;
  }
  // Se divide el numero entre el denominador adecuado para la cifra
  // se calcula el modulo 10 para obtener el digito
  for (int i = pow(10, cont); i>0; i/=10){
    printf("\n %i \n", mod(numero/i, 10));
  }
}
