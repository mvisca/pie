/**
@file filtros.h
@author M.Visca
@date 27/05/2017
@brief
Conjunto de funciones para filtrar imagenes.
*/
#ifndef FILTROS_H
#define FILTROS_H
/* Macros */
#define max(a,b) ((a > b) ? a:b)
#define min(a,b) ((a < b) ? a:b)
#define clipp(a, b, x) (min(a, max(b, x)))

/**
*Genera una copia una de la imagen.
*@param in_img: arreglo de pixeles a copiar
*@param width: ancho de la imagen
*@param heigth: altura de la imagen
*@param out_img: arreglo de pixeles, donde se realiza la copia.
*/
void copia(const Imagen* in_img, int width, int heigth, Imagen* out_img);

/**
*Genera el negativo de la imagen.
*@param in_img: arreglo de pixeles a calcular el negativo
*@param width: ancho de la imagen
*@param heigth: altura de la imagen
*@param maxval: maximo valor de pixel
*@param out_img: arreglo de pixeles, donde se almacena el negativo.
*/
void negativo(const Imagen *in_img, int width, int heigth, int maxval, Imagen *out_img);

/**
*Genera el reflejo de la imagen.
*@param in_img: arreglo de pixeles a calcular el reflejo
*@param width: ancho de la imagen
*@param heigth: altura de la imagen
*@param type: tipo de reflejo. 0: horizontal, 1: vertical, 2: central
*@param out_img: arreglo de pixeles de salida.
*/
void reflejo(const Imagen *in_img, int width, int heigth, int type, Imagen *out_img);

/**
Convierte los pixeles de una imagen a color a blanco y negro
*@param pin: puntero a la imagen de entrada
*@param pout: arreglo de pixeles en donde se almacena la conversion
*/
void rgb2bw(const Imagen *pin, Pixel *pout);

/**
*Reconoce los bordes mediante el uso de las derivadas en x e y, discretizadas
*@param in_img: arreglo de pixeles a procesar
*@param width: ancho de la imagen
*@param heigth: altura de la imagen
*@param maxval: maximo valor de pixel
*@param p: tipo de derivada. 0: horizontal, 1: vertical, 2: gradiente.
*@param out_img: arreglo de pixeles de salida.
*/
void bordes(Imagen *in_img, int width, int heigth, int maxval, int p, Imagen *out_img);

#endif
