/**
@file imagen.h
@author M. Visca
@date 27/05/2017
@brief Encabezado de la libreria imagen.
*/

#ifndef IMAGEN_H  /*Asegura la inclusión unica.*/
#define IMAGEN_H
/** Definición de MACROS:
RED: extrae el canal correspondiente al rojo desde un entero.
GREEN: idem.
BLUE: idem.
RGB2INT: dada la terna RGB convierte la misma a un entero.
*/
#define RED(x) ((x >> 16) & 255)
#define GREEN(x) ((x >> 8) & 255)
#define BLUE(x) ((x >> 0) & 255)
#define RGB2INT(r, g, b) (((r) << 16) | ((g) << 8) | (b))
#define MAXVAL 255

/**
Tipo Pixel
*/
typedef int Pixel;

/**
Enumerado con los posibles errores a retornar por las funciones escribir y leer imagen.
*/
enum codigo_error{
  PNM_OK = 0,
  PNM_ARCHIVO_INEXISTENTE,
  PNM_ERROR_LECTURA,
  PNM_ENCABEZADO_INVALIDO,
  PNM_DATOS_INVALIDOS,
  PNM_ERROR_ESCRITURA,
};

/**
Enumerado tipo_imagen, usado para identificar el tipo de imagen con la que se trabaja
*/
enum tipo_imagen{GRISES = 0, COLOR};

/*
Enumerado tipo canal, asigna un valor para cada canal.
*/
enum canal{ROJO = 0, VERDE, AZUL};

/**
Definiciones de tipo.
*/
typedef enum codigo_error CodigoError;

typedef enum tipo_imagen TipoImagen;

typedef enum canal Canal;

/**
Estructura donde se almacenara las imagenes.
*/
struct imagen{
  TipoImagen tipo;
  int ancho;
  int alto;
  int valor_maximo;
  Pixel *pixels;
};

typedef struct imagen Imagen;

/**
Inicializa la estructura correspondiente a la imagen. Setea los campos, tipo, ancho, alto, valor maximo = 255(defecto),
y reserva memoria para alojar los pixeles de la misma.
@param ancho: ancho de la imagen.
@param alto: alto de la imagen.
@param tipo: tipo de imagen a inicializar, {COLOR, GRISES}
@param pnew: puntero a la imagen.
*/
int inicializar_imagen(int ancho, int alto, TipoImagen tipo, Imagen* pnew);

/**
Destruye la imagen. Libera la memoria pedida para los pixeles. Setea el puntero a NULL, y los demas atributos a 0.
@param pimg: puntero a la estructura imagen a destruir.
*/
void destruir_imagen(Imagen* pimg);

/**
Dada una imagen de entrada, copia los atributos de la imagen de entrada a la de salida y le reserva memoria
para sus pixeles. SOLO reserva memoria, NO incializa los pixeles.
@param pin: puntero a la imagen de entrada
@param pout: puntero a la imagen de salida
*/
void duplicar_imagen(const Imagen* pin, Imagen* pout);

/**
Lee una imagen en blanco y negro (P2) o a color(P6). Retorna valores de un enumarado CodigoError, segun corresponda.
@param ruta: ruta a la imagen a leer.
@param pimg: puntero a la estructura imagen donde se almacenara la misma.
*/
CodigoError leer_imagen(const char* ruta, Imagen* pimg);

/**
Escribe una imagen a color o blanco y negro en disco. Retorna un CodigoError segun corresponda.
@param pimg: puntero a imagen a escribir.
@param ruta: ruta donde se escribira la imagen.
*/
CodigoError escribir_imagen(const Imagen* pimg, const char* ruta);
#endif
