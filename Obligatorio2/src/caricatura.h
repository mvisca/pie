/**
@file caricatura.h
@author Mario Visca
@date 01/06/2017
@brief Archivo de encabezado para caricatura.c
*/

#ifndef CARICATURA_H
#define CARICATURA_H
/**
Pregunta si las coordenas corresponden a bordes, si el pixel es borde, o si esta etiquetado devuelve su etiqueta.
@param pixs: arreglo de pixeles
@param m: fila de la matriz
@param n: columna de la matriz
@param width: ancho
@param heigth: alto
*/
int  islabeled(const int * pixs, int m , int n, int width, int heigth);

/**
Se encarga de re-etiquetar los pixeles desde un indice m hasta un indice n, la etiqueta lm por la ln.
@param pixels: arreglo de pixeles
@param m: punto de partida para el re-etiquetado
@param lm: etiqueta asociada al punto de partida
@param n: punto de llegada
@param ln: etiqueta asociada al punto de llegada
*/
void label(int *pixels, int m, int lm, int n, int ln);

/**
Dada una imagen de entrada, genera su version en "caricatura", bastante interesante.
@param pin: puntero a la imagen de entrada
@param width: ancho
@param heigth: alto
@param u: umbral, necesario para determinar que es borde y que no
@param pout: puntero a la imagen de salida
*/
void caricatura(Imagen *pin, int width, int heigth, int u, Imagen *pout);

#endif
