/**
@file filtro.c
@author M.Visca
@date 28/5/2017

MOD:
13/06/2017 -- cambie truncado por redondeo, en la conversion a BW (mejoro resultado)
*/

/* Librerias */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "imagen.h"

/* Macros */
#define max(a,b) ((a > b) ? a:b)
#define min(a,b) ((a < b) ? a:b)
#define clipp(a, b, x) (min(a, max(b, x)))


void copia(const Imagen* in_img, int width, int heigth, Imagen* out_img){
	int m, n;
  duplicar_imagen(in_img, out_img);

	for(m = 0; m < heigth; m++)
		for(n = 0; n < width; n++)
			out_img->pixels[m * width + n] = in_img->pixels[m * width + n];  /*Recorro y asigno pixel a pixel*/

}/*copia*/

void negativo(const Imagen *in_img, int width, int heigth, int maxval, Imagen *out_img){
	int i, tope;
  tope = width*heigth;
  duplicar_imagen(in_img, out_img);

  switch (in_img->tipo){
    case GRISES:
      for(i = 0; i < tope; i++)
		    out_img->pixels[i] = maxval - in_img->pixels[i];									/*Recorro y asigno la resta del maxval menos el pixel */
      break;
    case COLOR:
      for(i = 0; i < tope; i++)
        out_img->pixels[i] = RGB2INT(maxval - RED(in_img->pixels[i]), maxval - GREEN(in_img->pixels[i]), maxval - BLUE(in_img->pixels[i]));
      break;
  };
}/*negativo*/

void reflejo(const Imagen *in_img, int width, int heigth, int type, Imagen *out_img){
	int m, n, pivot;
  duplicar_imagen(in_img, out_img);
	if(type == 0){
    pivot = width/2;
		for(n = 0; n < pivot; n++){										/*Recorro hasta la mitad del ancho, pivot*/
			for(m = 0; m < heigth; m++){
				out_img->pixels[m*width + n] = in_img->pixels[m*width + (width -1 - n)];	/*A c/pixel le asigno el pixel opuesto*/
				out_img->pixels[m*width + (width -1 -n)] = in_img->pixels[m*width + n];		/* en la misma fila*/
			}
		}
	}
	else if(type == 1){
    pivot = heigth/2;
		for(m = 0; m < pivot; m++){								/*Recorro hasta la mitad de la altura, pivot*/
			for(n = 0; n < width; n++){
				out_img->pixels[m*width + n] = in_img->pixels[(heigth -1- m)*width + n];	/*A c/pixel le asigno el opuesto*/
				out_img->pixels[(heigth -1- m)*width + n] = in_img->pixels[m*width + n];	/*en la misma columna*/
			}
		}
	}
	else if(type == 2){
    pivot = heigth/2;
		for(m = 0; m < pivot; m++){							/*Recorro hasta la mitad de la altura, pivot*/
			for(n = 0; n < width; n++){
				out_img->pixels[m*width + n] = in_img->pixels[(heigth -1- m)*width + (width - 1 - n)]; /*A c/pixel le asigno el opuesto*/
				out_img->pixels[(heigth -1- m)*width + (width - 1 - n)] = in_img->pixels[m*width + n]; /*en la fila y col opuesta*/
			}
		}
	}
	else{
		printf("¡ERROR!, Parámetro inválido\a\n");
		exit(0);
	}
}/*reflejo*/

void rgb2bw(const Imagen *pin, Pixel *pout){
  int i;
  int tope = pin->ancho * pin->alto;

  for(i = 0; i < tope; i++){
    pout[i] = round(0.2*RED(pin->pixels[i]) + 0.7*GREEN(pin->pixels[i]) + 0.1*BLUE(pin->pixels[i]));
  }

}

void bordes(Imagen *in_img, int width, int heigth, int maxval, int p, Imagen *out_img){
	int m, n;
  Pixel *pixs;										/* Creo un puntero a un array de pixeles, me sirve para poder trabajar con ambos tipos de imagenes*/

  if(in_img->tipo == GRISES){						/*Si es una imagen P2, asigno al puntero pixs los pixeles de la imagen de entrada*/
    pixs = (in_img->pixels);
    duplicar_imagen(in_img, out_img);
  }
  else{
    inicializar_imagen(width, heigth, GRISES, out_img);	/*Debo incializar la salida como GRISES */
    pixs = (Pixel*) malloc(width*heigth*sizeof(int));		/* reservo memoria para el array de pixeles*/
    rgb2bw(in_img, pixs);																/* convierto la imagen a BW y guardo los pixeles en pixs */
  }

	/* Hago los calculos de igual manera que en el obligatorio 1 */
	for(m = 0; m < heigth; m++)
		for(n = 0; n < width; n++){
			float val = 0, vx = 0, vy = 0;					/*val: Valor del gradiente, vx = valor acumulado de la deriv respecto a x*/
											                        /*vy = valor acumulado de la derivada respecto a y*/
																							/*di, dj correciones de i y j para que se acceda bien a dx y dy*/
				int ii = clipp(heigth -1, 0, m+1);
				int iii = clipp(heigth -1, 0, m-1);

        int jj = clipp(width -1, 0, n+1);
        int jjj = clipp(width -1, 0, n-1);

				switch (p) {
					case  0:
            vx = pixs[m*width + (jj)] - pixs[m*width + (jjj)];	/*"Prod. convolutivo" de dx con la ventana de pixeles*/
						break;

					case 1:
            vy = pixs[(ii)*width + n] - pixs[(iii)*width + n];	/*"Prod. convolutivo" de dy con la ventana de pixeles*/
						break;

					case 2:
            vx = pixs[m*width + (jj)] - pixs[m*width + (jjj)];
						vy = pixs[(ii)*width + n] - pixs[(iii)*width + n];
						break;
					default:
						printf("¡ERROR! parametro invalido, verifique.\n");
						exit(0);
				} /*switch*/

			val = sqrt(vx*vx + vy*vy);																				/*Calculo y asignacion del gradiente*/

			out_img->pixels[m * width + n] = ( val > maxval) ? maxval:round(val);			/*si el valor del gradiente es mayor al max. val. de pixel se trunca a max. val.*/

		}/*for en n*/

	/* Si la imagen de entrada es a color, debo liberar la memoria que pedi para pixs */
	if(in_img->tipo == COLOR){
    free(pixs);
		pixs = NULL;
	}
} /*bordes*/
