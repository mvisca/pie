/**
@file main.c
@author Mario Visca
@date 27/05/2017
@brief funcion main del obligatorio, gestiona la lectura, escritura, y aplicado de los filtros.

ADD: 15/06/2017
  agregue exit(0) luego de retornar algun valor.
*/

/* Librerias */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "imagen.h"
#include "filtros.h"
#include "caricatura.h"

int main(int argc, char **argv){
  // chequeo de argumentos
  if(argc < 5 || argc >5){
    printf("ERROR: cantidad de argumentos inválida\n\n");
    printf("Uso: ./obligatorio arg1 arg2 arg3 arg4\n");
    printf("arg1: filtro\narg2: parametro\narg3: ruta a imagen de entrada\narg4: ruta a imagen de salida\n\n");
    printf("para mas información consultar la documentación.\n");
    exit(0);
  }

  // punteros a la imagen de entrada y salida
  Imagen *in_img, *out_img;
  // reservo memoria para las imagenes
  in_img = (Imagen*) malloc(sizeof(Imagen));
  out_img = (Imagen*) malloc(sizeof(Imagen));

  // leo la imagen de entrada
  // contemplo los posibles retornos de la funcion leer_imagen
  switch (leer_imagen(argv[3], in_img)){
    case 0:
      printf("Imagen leida correctamente\n");
      break;
    case 1:
      printf("Archivo inexistente\n");
      exit(0);
      break;
    case 2:
      printf("Error de lectura\n");
      exit(0);
      break;
    case 3:
      printf("Encabezado inválido\n");
      exit(0);
      break;
    case 4:
      printf("Datos inválidos\n");
      exit(0);
      break;
    case 5:
      printf("Esto no debia suceder...\n");
      exit(0);
      break;
  };

  // llamadas a los filtros
  if(strcmp(argv[1], "copia") == 0){
    copia(in_img, in_img->ancho, in_img->alto, out_img);
  }
  else if(strcmp(argv[1], "negativo") == 0){
    negativo(in_img, in_img->ancho, in_img->alto, in_img->valor_maximo, out_img);
  }
  else if(strcmp(argv[1], "reflejo") == 0){
    reflejo(in_img, in_img->ancho, in_img->alto, atoi(argv[2]), out_img);
  }
  else if(strcmp(argv[1], "bordes") == 0){
    bordes(in_img, in_img->ancho, in_img->alto, in_img->valor_maximo, atoi(argv[2]), out_img);
  }
  else if (strcmp(argv[1], "caricatura") == 0){
    caricatura(in_img, in_img->ancho, in_img->alto, atoi(argv[2]), out_img);
  }

  // Escribo la imagen
  // retorno el codigo que corresponda
  switch (escribir_imagen(out_img, argv[4])){
    case 0:
      printf("Imagen escrita correctamente...\n");
      break;
    case 1:
      printf("No se ha podido abrir el archivo...\n");
      exit(0);
      break;
    case 2:;
    case 3:;
    case 4:;
      printf("Esto no debia suceder...\n");
      exit(0);
      break;
    case 5:
      printf("Error al escribir la imagen...\n");
      exit(0);
      break;
  };

  // libero la memoria reservada para los pixeles de las imagenes
  // y para las imagenes en si.
  destruir_imagen(in_img);
  destruir_imagen(out_img);
  free(in_img);
  free(out_img);

}
