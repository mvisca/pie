/**
@file caricatura.c
@author Mario Visca
@date 01/06/2017
@brief fuente del filtro caricatura.

MOD: 15/06/2017
  Modifique la manera de acumular y generar los promedios y rellenar las regiones con el promedio
  correspondiente, elimine la función fill.
  Mediante un nuevo Array(R_acum) de estructura meta, y aprovechando las iteraciones.
  Referencia con imagen bike.ppm. y umbral 2:
    Tiempo con procesamiento antes: indeterminado (luego de 1.40h no habia terminado)
    Ahora: 0.950 segundos
*/

/* Librerias */
#include <stdlib.h>
#include <stdio.h>
#include<math.h>
#include "imagen.h"
#include "filtros.h"
#include "caricatura.h"
/* Nada mas que una abreviatura para evitar calentarme al escribir lo mismo */
#define  PIXEL pout->pixels

struct meta{
  int npixs;
  float bw;
  float r;
  float g;
  float b;
};

typedef struct meta Meta;

int  islabeled(const int * pixs, int m , int n, int width, int heigth){
  if( n < 0 || m < 0){                        /* Si las cordenas n y m son < 0, me fui de rango => son bordes */
    return 0;
  }else if(pixs[m*width + n] == 0){           /* Pregunto si el pixel correspondiente a las coords es borde */
    return 0;
  }else{
    return pixs[m*width + n];                 /* Si no, retorno la etiqeta que tenga */
  }
}/* islabeled */

void label(int *pixels, int m, int lm, int n, int ln){
  for(int i = m; i <= n; i++)                 /* Recorro desde un indice dado hasta otro */
    if(pixels[i] == lm)                       /* remplazo solo las etiquetas de interes(lm) por las (ln) */
      pixels[i] = ln;
}/*label*/

void caricatura(Imagen *pin, int width, int heigth, int u, Imagen *pout){

  int tope = pin->ancho * pin->alto;
  /* Aplico el filtro bordes con parametro 2 y lo guardo en la imagen de salida */
  bordes(pin, pin->ancho, pin->alto, MAXVAL, 2, pout);
  for(int i = 0; i < tope; i++){                             /* Mato aquellos pixeles que sean menores que el umbral */
    PIXEL[i] = (PIXEL[i] > u) ? 0 : -1;
  }
  pout->tipo = (pin->tipo == COLOR) ? COLOR: GRISES;        /* Como la funcion bordes retorna siempre una imagen en BW,*/
                                                            /* segun sea necesario */

  int *R_index = (int*) malloc(tope*sizeof(int));           /* En este array almaceno los indices donde se crean por primera ves las etiquetas */
  int R = 0;                                                /* Contador de Regiones */

  for(int i = 0; i < heigth; i++){                          /* Recorro la imagen */
    for(int j = 0; j < width; j++){

      if (PIXEL[i*width + j] != 0){                              /* Si el píxel actual != 0, hay que etiquetarlo */
        int north = islabeled(PIXEL, i-1, j, width, heigth);     /* pregunto si el norte y el este son bordes o si estan etiquetados */
        int west = islabeled(PIXEL, i, j-1, width, heigth);
        if (!north && !west){                                    /*Si ambos son bordes, creo una region nueva, etiqueto el pixel */
          R++;
          PIXEL[i*width + j] = R;
          R_index[R] = i*width+j;                                /* Registro el indice donde se creo la etiqueta */

        }else{
          if (north && !west){                                   /* Si el norte esta etiquetado, asigno su etiqueta al actual */
            PIXEL[i*width + j] = north;

          }else{
            if(!north && west){                                  /* Idem pero con el este */
              PIXEL[i*width +j] = west;

            }else{
              if(north == west){                                 /* Si ambos estan etiquedos y son iguales, asigno la misma etiqueta */
                PIXEL[i*width + j] = north;

              }else{
                if( north != west){                                           /* Si sus etiquetas son distintas */
                  if( R_index[north] < R_index[west]){                        /* Debo considerar que etiqueta en conflicto se creo primero */
                    PIXEL[i*width + j] = north;                               /* para eso me sirve mirar el registro de indices */
                    label(PIXEL, R_index[west], west, i*width+j, north);      /* remplazo todas las etiquetas en conflicto por la que aparecio primero */
                    R_index[west] = R_index[north];                           /* Debo actualizar los indices */
                  }else{
                    PIXEL[i*width + j] = west;
                    label(PIXEL, R_index[north], north, i*width+j, west);
                    R_index[north] = R_index[west];
                  }
                }
              }
            }
          }
        }
      } /* if pixel != 0*/
    } /* for j */
  } /*for i */

  /* Una vez etiquetada la imagen, la recorro para obtener la cantidad de pixeles que caen en cada region,
   y obtener el acumulado de los pixeles para luego hacer la media, y asignarla a la region
  */
  Meta *R_acum = (Meta*) malloc(tope*sizeof(Meta));


  for(int l = 0; l < tope; l++){
    R_acum[l].npixs = 0;
    // R_acum[l].bw = 0;
    R_acum[l].r = 0;
    R_acum[l].g = 0;
    R_acum[l].b = 0;
  }

  for(int l = 0; l < tope; l++){
    int label = 0;
    label = PIXEL[l];
    if(label != 0){
      R_acum[label].npixs +=1;

      if(pin->tipo == COLOR){
        R_acum[label].r += RED(pin->pixels[l]);
        R_acum[label].g += GREEN(pin->pixels[l]);
        R_acum[label].b += BLUE(pin->pixels[l]);
      }else{
        R_acum[label].bw += pin->pixels[l];
      }
    }
  }
  //
  for(int l = 0; l < tope; l++){
    int label = PIXEL[l];
    if(label != 0){
      if(pin->tipo == COLOR){
        float red = R_acum[label].r / R_acum[label].npixs;
        float green = R_acum[label].g / R_acum[label].npixs;
        float blue = R_acum[label].b / R_acum[label].npixs;

        PIXEL[l] = RGB2INT((int) round(red), (int) round(green), (int) round(blue));
      }else{
        PIXEL[l] = round(R_acum[label].bw / R_acum[label].npixs);
      }
    }
  }

  /* Libero la memoria que pedi para almacenar los indices de las regiones */
  free(R_index);
  free(R_acum);
  R_index = NULL;
  R_acum = NULL;
}/*caricatura*/
