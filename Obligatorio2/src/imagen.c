/**
@file imagen.c
@author M. Visca
@date 27/05/2017
@brief Libreria para lectura y escritura de imagenes pgm(P2) y ppm(P6)

FIX: 15/06/2017
  pregunto que se hayan leido los 4 valores de encabezado,
  testeo el caso que maxval > 255 (Soluciono conditional jump, valgrind leak).
*/

// Librerias
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "imagen.h"

/**
Inicializa la estructura correspondiente a la imagen. Setea los campos, tipo, ancho, alto, valor maximo = 255(defecto),
y reserva memoria para alojar los pixeles de la misma.
@param ancho: ancho de la imagen.
@param alto: alto de la imagen.
@param tipo: tipo de imagen a inicializar, {COLOR, GRISES}
@param pnew: puntero a la imagen.
*/
int inicializar_imagen(int ancho, int alto, TipoImagen tipo, Imagen* pnew){
  pnew->pixels = (Pixel*) malloc(ancho*alto*sizeof(int));

  if(pnew->pixels == NULL){
    printf("ERROR: No se ha podido reservar memoria.");
    return 1;
  }
  pnew->ancho = ancho;
  pnew->alto = alto;
  pnew->valor_maximo = MAXVAL;
  pnew->tipo = tipo;

  return 0;
 }
/**
Destruye la imagen. Libera la memoria pedida para los pixeles. Setea el puntero a NULL, y los demas atributos a 0.
@param pimg: puntero a la estructura imagen a destruir.
*/
void destruir_imagen(Imagen* pimg){
  free(pimg->pixels);
  pimg->pixels = NULL;
  pimg->alto = 0;
  pimg->ancho = 0;
  pimg->valor_maximo = 0;
}

/**
Dada una imagen de entrada, copia los atributos de la imagen de entrada a la de salida y le reserva memoria
para sus pixeles. SOLO reserva memoria, NO incializa los pixeles.
@param pin: puntero a la imagen de entrada
@param pout: puntero a la imagen de salida
*/
void duplicar_imagen(const Imagen* pin, Imagen* pout){
  pout->tipo = pin->tipo;
  pout->alto = pin->alto;
  pout->ancho = pin->ancho;
  pout->valor_maximo = pin->valor_maximo;

  pout->pixels = (Pixel*) malloc((pout->alto)*(pout->ancho)*sizeof(int));
}

/**
Lee una imagen en blanco y negro (P2) o a color(P6). Retorna valores de un enumarado CodigoError, segun corresponda.
@param ruta: ruta a la imagen a leer.
@param pimg: puntero a la estructura imagen donde se almacenara la misma.
*/
CodigoError leer_imagen(const char* ruta, Imagen* pimg){
  char str_tipo[2];
  int npixels, ancho, alto, maxval, tope;
  TipoImagen tipo;
  npixels = 0;

  FILE *f;
  f = fopen(ruta, "r");

  if(f == NULL){
    return PNM_ARCHIVO_INEXISTENTE;
  }

  if(fscanf(f, "%s\n%d %d\n%d\n", str_tipo, &ancho, &alto, &maxval) != 4){
    fclose(f);
    return PNM_ENCABEZADO_INVALIDO;
  }

  // chequeo el tipo de imagen
  if(strcmp(str_tipo, "P2") == 0){
    tipo = GRISES;
  }else if(strcmp(str_tipo, "P6") == 0){
    tipo = COLOR;
  }else{
    fclose(f);
    return PNM_ENCABEZADO_INVALIDO;
  }
  // chequeo las dimensiones
  if(ancho <= 0 || alto <= 0 || maxval <= 0 || maxval > 255){
    fclose(f);
    return PNM_ENCABEZADO_INVALIDO;
  }
  inicializar_imagen(ancho, alto, tipo, pimg);               //Incializo la imagen

  int i = 0;
  int pixel;
  tope = ancho*alto;
  switch (tipo){
    case GRISES:
      while(i < tope){
        npixels = fscanf(f, "%d", &pixel);
        pimg->pixels[i] = pixel;

        if(ferror(f)){
          fclose(f);
          destruir_imagen(pimg);
          return PNM_ERROR_LECTURA;
        }
        else if(feof(f)){
          fclose(f);
          destruir_imagen(pimg);
          return PNM_DATOS_INVALIDOS;
        }
        i++;
      }
      break;

    case COLOR:
      while(npixels < tope){

        pimg->pixels[npixels] = RGB2INT( fgetc(f), fgetc(f), fgetc(f));

        if(ferror(f)){
          fclose(f);
          destruir_imagen(pimg);
          return PNM_ERROR_LECTURA;
        }
        else if (feof(f)){
          fclose(f);
          destruir_imagen(pimg);
          return PNM_DATOS_INVALIDOS;
        }
        npixels++;
      }
      break;
  };
  fclose(f);
  return PNM_OK;
}
/**
Escribe una imagen a color o blanco y negro en disco. Retorna un CodigoError segun corresponda.
@param pimg: puntero a imagen a escribir.
@param ruta: ruta donde se escribira la imagen.
*/
CodigoError escribir_imagen(const Imagen* pimg, const char* ruta){
  FILE *f;

  f = fopen(ruta, "w");
  if(f == NULL)
    return PNM_ARCHIVO_INEXISTENTE;

  int i = 0, tope;
  char *str_tipo = (pimg->tipo == GRISES) ? "P2" : "P6";

  fprintf(f, "%s\n%d %d\n%d\n", str_tipo, pimg->ancho, pimg->alto, pimg->valor_maximo);
  tope = (pimg -> ancho)*(pimg->alto);
  switch (pimg->tipo){
    case 0:
      while(i < tope){
        if(fprintf(f, "%d ", pimg->pixels[i]) < 0){
          fclose(f);
          return PNM_ERROR_ESCRITURA;
        }
        i++;
      }
      break;
    case 1:
      while(i < tope){

        fputc(RED(pimg->pixels[i]), f);
        fputc(GREEN(pimg->pixels[i]), f);
        fputc(BLUE(pimg->pixels[i]), f);

        if(ferror(f)){
          fclose(f);
          return PNM_ERROR_ESCRITURA;
        }
        i++;
      }
      break;
  };

  fclose(f);
  return PNM_OK;
}
